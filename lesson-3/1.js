/**
 * Задача 1.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо преобразовать первый символ переданной строки в заглавный.
 *
 * Условия:
 * - Необходимо проверить что параметр str является строкой
 */

function upperCaseFirst(str) {
    // str ← строка которая в нашем случае равна 'pitter' или ''
    
    // РЕШЕНИЕ НАЧАЛО
    if (typeof str !== 'string')
        return '';
    const firstChar = str.charAt(0);
    const result = firstChar.toUpperCase() + str.slice(1);
    // РЕШЕНИЕ КОНЕЦ
    
    return result;
}

console.log(upperCaseFirst('pitter')); // Pitter
console.log(upperCaseFirst('')); // ''
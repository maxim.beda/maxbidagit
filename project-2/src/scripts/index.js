import '../styles/index.scss';
import { tarifs } from './constants';
let balance = 100;
console.log(tarifs);

// payment
const payments = [];
const payment = {
    id: 'water',
    meterId: 'DS949321',
    current: null,
    previous: null,
    amount: null
};

// elements
const left__company = document.querySelectorAll('.left__company');
const center__title = document.querySelector('.center__title');

// forms
const center__form = document.querySelector('.center__form');
const form__summary_list = document.querySelector('.form__summary-list');
const meters = document.getElementById('meters');
const previous = document.getElementById('previous');
const current = document.getElementById('current');
const total = document.getElementById('total');

const right__payments = document.querySelector('.right__payments');
const checks = Array.from(document.querySelectorAll(`.right__payments input[type="checkbox"]`));


// validate form
const validateNumbers = () => {
    const currVal = +current.value;
    const prevVal = +previous.value;

    if (isNaN(currVal) || isNaN(prevVal) || currVal <= 0 || prevVal <= 0) {
        alert('The values must be a positive number');
        return false;
    }
    if (currVal <= prevVal) {
        alert('Previous value must be less than Current value');
        return false;
    }
    payment.current = currVal;
    payment.previous = prevVal;
    payment.amount = +((currVal - prevVal) * tarifs[payment.id]).toFixed(2);
    return true;
};

// new payment
const newPayment = payment => {
    const { meterId, amount } = payment;
    const paymentHTML = `
        <li class="list__item">
            <p>
                <span class="list__item-label">${meterId}</span>
                <span class="price">$ <b>${amount}</b></span>
            </p>
        </li>`;
    form__summary_list.insertAdjacentHTML('afterbegin', paymentHTML);
};

// save payment
const savePayment = () => {
    const check = checks.find(x => x.dataset.for === payment.id);
    check.checked = true;
};

// clear payment
const clearPayment = () => {
    payment.current = null;
    payment.previous = null;
    payment.amount = null;
};

// clear payments list
const clearPaymentsList = () => {
    const list__item = document.querySelectorAll('.list__item:not(.list__total)');
    list__item.forEach(item => {
        item.remove();
    });
    total.innerText = 0;
};

// total to pay
const totalPayment = payment => {
    const sum = +total.innerText;
    total.innerText = sum + payment.amount;
};

// payment change
left__company.forEach(element => {
    element.addEventListener('click', e => {
        const current = e.currentTarget;
        const id = current.dataset.id;
        const selected = document.querySelector('.left__company.selected');
        selected.classList.remove('selected');
        current.classList.add('selected');
        center__title.innerText = current.innerText;
        payment.id = id;
        console.log(payment);
    });
});

// meters change
meters.addEventListener('change', () => {
    payment.meterId = meters.value;
    console.log(payment);
});

// center form submit
center__form.addEventListener('submit', e => {
    e.preventDefault();
    if(!validateNumbers()) {
        return false;
    }

    // clone payment and push to array
    const clone = Object.assign({}, payment);
    payments.push(clone);
    console.log(payments);

    // update payments list
    newPayment(payment);

    // update total
    totalPayment(payment);

    // save payment
    savePayment();

    // clear payment
    clearPayment();
});

// ceenter form reset
center__form.addEventListener('reset', e => {
    // refresh HTML
    clearPaymentsList();

    // clean payments
    payments.length = 0;
    clearPayment();
    console.log(payments);

    // clean saved payments
    checks.forEach(check => {
        check.checked = false;
    });
});

// right form submit
right__payments.addEventListener('submit', e => {
    e.preventDefault();
    const checked = checks.filter(x => x.checked === true);

    // update payments
    checked.forEach(check => {
        console.log(`${check.dataset.for} - paid`);
        for (let i = 0; i < payments.length; i++) { 
            if (payments[i].id === check.dataset.for) {
                payments.splice(i, 1);
                i--; 
            }
        }
    });
    console.log(payments);

    // re-render payments list
    clearPaymentsList();
    payments.forEach(payment => {
        newPayment(payment);
        totalPayment(payment);
    });
});
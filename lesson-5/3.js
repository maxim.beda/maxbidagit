/**
 * Задача 3.
 *
 * Создайте функцию `isEven`, которая принимает число качестве аргумента.
 * Функция возвращает булевое значение.
 * Если число, переданное в аргументе чётное — возвращается true.
 * Если число, переданное в аргументе нечётное — возвращается false.
 *
 * Условия:
 * - Генерировать ошибку, если в качестве входного аргумента был передан не числовой тип;
 */

// РЕШЕНИЕ
const isEven = function(num){
    if (typeof num !== "number" || isNaN(num))
        throw new Error('the num is not a number');
    console.log(num % 2 === 0);
}

isEven(3); // false
isEven(4); // true
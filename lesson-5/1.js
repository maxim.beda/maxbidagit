/**
 * Задача 1.
 *
 * Создайте функцию `f`, которая возвращает куб числа, переданного в качестве аргумента.
 *
 * Условия:
 * - Генерировать ошибку, если в качестве аргумента был передан не числовой тип.
 */

// РЕШЕНИЕ
const f = function(num){
    if (typeof num !== "number" || isNaN(num))
        throw new Error('the num is not a number');
    return num * num * num;
}

console.log(f(2)); // 8

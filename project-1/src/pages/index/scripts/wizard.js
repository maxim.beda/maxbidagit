import { saveToStorage } from './login';
console.log('wizard');

//next/prev steps
const showBlock = block => {
    for (const block of allBlocks) {
        block.style.display = 'none';
    }
    block.style.display = null;
};

//TASK 1: wizard

//step 0
const loginBtn = document.getElementById('loginBtn');
const regBtn = document.getElementById('regBtn');

//step 1
const toLoginSvg = document.getElementById('toLoginSvg');
const from1to2 = document.getElementById('from1to2');
const toStep2Btn = document.getElementById('toStep2Btn');

//step 2
const from3to2Svg = document.getElementById('from3to2Svg');
const from2to1 = document.getElementById('from2to1');
const createAccount = document.getElementById('createAccount');

//blocks
const loginBlock = document.getElementById('loginBlock');
const step1Block = document.getElementById('step1Block');
const regBlock = document.getElementById('regBlock');
const allBlocks = document.querySelectorAll('.block__login');

//step 0 listeners
regBtn.addEventListener('click', () => {
    showBlock(step1Block);
});

//step 1 listeners
toLoginSvg.addEventListener('click', () => {
    showBlock(loginBlock);
});
[from1to2, toStep2Btn].forEach(elem => {
    elem.addEventListener('click', () => {
        showBlock(regBlock);
    });
});

//step 2 listeners
[from3to2Svg, from2to1].forEach(elem => {
    elem.addEventListener('click', () => {
        showBlock(step1Block);
    });
});

//TASK 2: new user

//user type
const user = { type: 'student' };
const userStudent = document.getElementById('user_student');
const userTeacher = document.getElementById('user_teacher');

//user/teacher listeners
userStudent.onchange = () => {
    user.type = 'student';
};
userTeacher.onchange = () => {
    user.type = 'teacher';
};

//registration
const loginForm = document.querySelector('#regBlock .form-login');
const name = document.getElementById('name');
const email = document.getElementById('email');
const password = document.getElementById('password');
const passwordNext = document.getElementById('password_next');

const validateForm = () => {
    let passed = true;
    for(const input of loginForm.elements) {
        //reset error
        input.addEventListener('change', () => {
            input.classList.remove('error');
        });
        //check blank value
        if(input.value.trim() === '') {
            console.log(`"${input.name}" can not be empty`);
            input.classList.add('error');
            passed = false;
        }
    }
    return passed;
};
const validateName = () => {
    const nameArray = name.value.split(' ');
    if (nameArray.length < 2 || nameArray[0].length < 3 || nameArray[1].length < 3) {
        console.log('the "name" is not valid');
        name.classList.add("error");
        return false;
    }
    return true;
};
const validatePassword = () => {
    if (password.value !== passwordNext.value) {
        console.log('"password" does not match');
        password.classList.add("error");
        passwordNext.classList.add("error");
        return false;
    }
    return true;
};
const onSubmit = () => {
    if (!validateForm() || !validateName() || !validatePassword()) {
        return false;
    }
    user.name = name.value;
    user.email = email.value;
    user.password = password.value;
    
    if (user.type === 'teacher') {
        localStorage.setItem('teacher', JSON.stringify(user));
    }
    else if (user.type === 'student') {
        const arr = JSON.parse(localStorage.getItem('students') || '[]');
        arr.push(user);
        localStorage.setItem('students', JSON.stringify(arr));
    }
    else {
        return console.log('something went wrong...');
    }
    
    console.log(user);
    saveToStorage(user);
    window.location.href = `${user.type}.html`;
};

//registration submit
createAccount.addEventListener('click', () => {
    onSubmit();
});
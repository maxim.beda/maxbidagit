console.log('planed lessons');

//TASK 4: generate lessons

const planedLessons = document.getElementById('planedLessons');
const savedLessons = JSON.parse(localStorage.getItem('lessons')) || [];

const getLesson = (lesson) => {
    const { name, title, time, tomorrow, duration } = lesson;
    let d = new Date();
    d.setHours(time, 0);
    d.setMinutes(duration);
    const timeStart = `${time}:00`;
    const timeEnd = d.getHours() + ':' + (d.getMinutes() < 9 ? '0' : '') + d.getMinutes();

    return `
        <div class="card-box">
            <div class="card-illustration">
                <img src="./images/profile.svg" alt="">
            </div>
            <div class="info">
                <p class="sub-title">${tomorrow ? 'Завтра' : 'Сегодня'}, ${timeStart} — ${timeEnd}</p>
                <p class="info-title">${name}</p>
                <p class="info-desc">${title}</p>
            </div>
        </div>`;
};

const generateHTML = () => {
    const lessonsHTML = savedLessons.map((lesson) => {
        return getLesson(lesson);
    }).join('');
    
    planedLessons.insertAdjacentHTML('afterend', lessonsHTML);
};

generateHTML();
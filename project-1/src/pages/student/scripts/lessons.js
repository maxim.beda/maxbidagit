import { timeSlots, lessons } from './constants';
console.log('lessons form');

//TASK 3: lessons

const login = JSON.parse(localStorage.getItem('login') || '{}');
const lesson = {
    name: login.name,
    title: lessons.type_01.title,
    time: timeSlots.time_01,
    tomorrow: false,
    duration: lessons.type_01.duration
};
const formLessons = document.getElementById('formLessons');

formLessons.addEventListener('submit', (e) => {
    e.preventDefault();
    //title
    const type = document.querySelector('input[name="type"]:checked');
    lesson.title = lessons[type.id].title;
    lesson.duration = lessons[type.id].duration;
    //time
    const time = document.querySelector('input[name="time"]:checked');
    lesson.time = timeSlots[time.id];
    const i = +time.id.slice(-1);
    (i > 3) ? lesson.tomorrow = true : lesson.tomorrow = false;

    const arr = JSON.parse(localStorage.getItem('lessons') || '[]');
    arr.push(lesson);
    localStorage.setItem('lessons', JSON.stringify(arr));
});
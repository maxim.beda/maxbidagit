/**
 * Создать форму динамически при помощи JavaScript.
 * 
 * В html находится пример формы которая должна быть сгенерирована.
 * 
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 * 
 * Обязательно!
 * 1. Для генерации элементов обязательно использовать метод document.createElement
 * 2. Для установки атрибутов элементам обязательно необходимо использовать document.setAttribute
 * 3. Всем созданным элементам необходимо добавить классы как в разметке
 * 4. После того как динамическая разметка будет готова необходимо удалить код в HTML который находится между комментариями
*/

// РЕШЕНИЕ
const fragment = document.createDocumentFragment();
const createElement = (tag, attr, parent = fragment, content = null) => {
    const e = document.createElement(tag);
    e.innerHTML = content;
    for (let key of Object.keys(attr)) {
        e.setAttribute(key, attr[key]);
    }
    parent.append(e);
    return e;
}

const email = createElement('div', { class: 'form-group' });
createElement('label', { for: 'email' }, email, 'Электропочта');
createElement('input', { id: 'email', class: 'form-control', type: 'text', placeholder: 'Введите свою электропочту' }, email);

const pass = createElement('div', { class: 'form-group' });
createElement('label', { for: 'password' }, pass, 'Пароль');
createElement('input', { id: 'password', class: 'form-control', type: 'password', placeholder: 'Введите пароль' }, pass);

const check = createElement('div', { class: 'form-group form-check' });
createElement('input', { id: 'exampleCheck1', class: 'form-check-input', type: 'checkbox' }, check);
createElement('label', { for: 'exampleCheck1', class: 'form-check-label' }, check, 'Запомнить меня');

createElement('button', { class: 'btn btn-primary', type: 'submit' }, fragment, 'Вход');

const form = document.getElementById('form');
form.append(fragment);

form.addEventListener('submit', function (e) {
    e.preventDefault();
    const email = document.getElementById('email').value.trim();
    const pass = document.getElementById('password').value.trim();
    const remember = document.getElementById('exampleCheck1').checked;
    if (!email || !pass) {
        console.log('bad submit');
    }
    const result = {
        email: email,
        password: pass,
        remember: remember
    }
   console.log(result);
});
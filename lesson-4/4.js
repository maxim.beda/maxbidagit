/**
 * Задача 4.
 *
 * Напишите функцию `collect`, которая будет принимать массив в качестве аргумента.
 * Возвращаемое значение функции — число.
 * Массив, который передаётся в аргументе может быть одноуровневым или многоуровневым.
 * Число, которое возвращает функция должно быть суммой всех элементов
 * на всех уровнях всех вложенных массивов.
 *
 * Если при проходе всех уровней не было найдено ни одного числа,
 * то функция должна возвращать число 0.
 *
 * Условия:
 * - Обязательно использовать встроенный метод массива reduce.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - Если на каком-то уровне было найдено не число и не массив.
 */

// Решение

const array1 = [[[1, 2], [1, 2]], [[2, 1], [1, 2]]];
console.log(collect(array1)); // 12

const array2 = [[[[[1, 2]]]]];
console.log(collect(array2)); // 3

const array3 = [[[[[1, 2]]], 2], 1];
console.log(collect(array3)); // 6

const array4 = [[[[[]]]]];
console.log(collect(array4)); // 0

const array5 = [[[[[], 3]]]];
console.log(collect(array5)); // 3

const array6 = [1,"test", 2,3,4];
console.log(collect(array6)); // 3



function collect(array) {
    if(!Array.isArray(array))
        return 'array is not an Array'
    let result = 0;
    const newArr = array.join().split(',');
    result = newArr.reduce(function(acc, curr, i, arr) {
        const num = +curr;
        if (isNaN(num)) {
            arr.splice(0);
            return 'array element is not a Number';
        }
        return acc + num;
    }, 0);
    return result;
}

//exports.collect = collect;
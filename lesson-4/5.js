/**
 * Задача 5.
 *
 * Напишите функцию `createArray`, которая будет создавать массив с заданными значениями.
 * Первым параметром функция принимает значение, которым заполнять массив.
 * А вторым — количество элементов, которое должно быть в массиве.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента были переданы не число, не строка, не объект и не массив;
 * - В качестве второго аргумента был передан не число.
 */

// Решение
function createArray(value, num) {
    if (!value || typeof num !== "number")
        return 'parameters are wrong';
    const arr = new Array(num).fill(value);
    return arr;
}

const result = createArray('x', 5);
const result1 = createArray(undefined, 5);
const result2 = createArray('x', 'y');

console.log(result); // [ x, x, x, x, x ]
console.log(result1);
console.log(result2);

//exports.createArray = createArray;